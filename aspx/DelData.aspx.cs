﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class aspx_DelData : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if(Request.Cookies["login"]==null)
        {
            Response.Redirect("../aspx/Login.aspx");
        }
        try
        {
            string email = Request.Cookies["login"].Value;
            int id = int.Parse(Request.QueryString["id"]);
            int kind = int.Parse(Request.QueryString["kind"]);
            switch (kind)
            {
                case 1:
                    {
                        SqlConnection cnn = new SqlConnection(SqlHelper.CONN_STRING);
                        cnn.Open();
                        string cmdStr = "Delete from IncomeRecord where " +
                            "Email='" + email + "'" + "and Id='" + id.ToString() + "'";
                        SqlCommand cmd = new SqlCommand(cmdStr, cnn);
                        if (cmd.ExecuteNonQuery() == 0)
                        {
                            Response.Redirect("table.aspx");
                        }
                        cnn.Close();
                        Response.Redirect("table.aspx");
                        break;
                    }
                case 2:
                    {
                        SqlConnection cnn = new SqlConnection(SqlHelper.CONN_STRING);
                        cnn.Open();
                        string cmdStr = "Delete from ExpendRecord where " +
                            "Email='" + email + "'" + "and Id='" + id.ToString() + "'";
                        SqlCommand cmd = new SqlCommand(cmdStr, cnn);
                        if (cmd.ExecuteNonQuery() == 0)
                        {
                            Response.Redirect("table.aspx");
                        }
                        cnn.Close();
                        Response.Redirect("table.aspx");
                        break;
                    }
            }


        }
        catch (Exception er)
        {
            Response.Write(er);
            //Response.Redirect("failedOp.aspx");
        }
    }
}