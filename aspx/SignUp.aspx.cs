﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Web.Security;

public partial class aspx_SignUp : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            /* 第一次进入注册页面*/ 
        
        }
        else
        {    /* 提交信息*/
            string strCnn = ConfigurationManager.ConnectionStrings["YunDBcstr"].ConnectionString;
            SqlConnection cnn = new SqlConnection(strCnn);
            try
            {
                cnn.Open();
                SqlCommand cmdRegister = new SqlCommand();
                cmdRegister.Connection = cnn;
                ///先找是否存在该用户，若存在则进行添加，否则提示错误
                string _email = Request.Form["signup_email"];
                string _pw = Request.Form["signup_pw"];
                string _fullname = Request.Form["signup_fullname"];
                cmdRegister.CommandText = "select count(*) from UserInfo where Email='" + _email + "'";
                ///ExecuteScalar方法执行单值查询
                object count = cmdRegister.ExecuteScalar();
                if ((int)count != 0)
                 {
                    ClientScript.RegisterStartupScript(ClientScript.GetType(),"login", "<script>  var a = document;b = a.getElementById('login_email'); login.showTip('该邮箱已注册',b.nextElementSibling);</script>");
                }
                 else
                 {
                      string password = FormsAuthentication.HashPasswordForStoringInConfigFile(Request.Form["signup_pw"].Trim(), "md5");
                  cmdRegister.CommandText = "insert into UserInfo(Email,Userpw,UserFullName) values('" + Request.Form["signup_email"].Trim()
                  + "','" + password + "','" + Request.Form["signup_fullname"].Trim() + "')";
                      ///使用ExecuteNonQuery修改或删除数据
                  int sucess = cmdRegister.ExecuteNonQuery();
                  if (sucess == 1)
                  {
                      HttpCookie cookieLogin = new HttpCookie("login");
                      cookieLogin.Value = Request.Form["signup_email"];
                      cookieLogin.Expires = System.DateTime.Now.AddDays(7);
                      Response.Cookies.Add(cookieLogin);
                      Response.Redirect("statistics.aspx");
                  }
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            //Response.Write("connection sucecced!");
            finally
            {
                if (cnn.State != ConnectionState.Closed)
                {
                    cnn.Close();
                }
            }
        }
    }
}