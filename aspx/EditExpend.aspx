﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditExpend.aspx.cs" Inherits="aspx_EditExpend" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
 <meta charset="utf-8" />
<link rel="icon" type="image/png" href="assets/Img/favicon.ico"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="../assets/css/MasterPage/animate.min.css" rel="stylesheet" />
    <link href="../assets/css/MasterPage/bootstrap.min.css" rel="stylesheet" />
    <link href="../assets/css/MasterPage/light-bootstrap-dashboard.css" rel="stylesheet" />
    <link href="../assets/css/MasterPage/pe-icon-7-stroke.css" rel="stylesheet" />
    <title></title>
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="../assets/Img/sidebar-7.jpg">
    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                云端理财
                </a>
            </div>
            <div class="userimg">
                <div class="profile-main">
                    <div class="profile-pic">
         
                       <ul class="nav">
                           <li id="user_name" class="simple-text">                             
                           你的名字
                           </li>
                           <li>
                               <div style=" margin:10px auto;"><img src="../assets/Img/tim_80x80.png" style=" border-radius:50%"/></div>
                           </li>
                           <li class="simple-text">
                               Welcome
                           </li>
                       </ul>
                  
                    </div>
                </div>
            </div>

            <ul class="nav">
                <li>
                    <a href="statistics.aspx">
                        <i class="pe-7s-graph"></i>
                        <p>统计功能</p>
                    </a>
                </li>
                <li>
                    <a href="user.aspx">
                        <i class="pe-7s-user"></i>
                        <p>个人设置</p>
                    </a>
                </li>
                <li class="active">
                    <a href="table.aspx">
                        <i class="pe-7s-note2"></i>
                        <p>账单</p>
                    </a>
                </li>
            </ul>
    	</div>
        <div class="sidebar-background" style="background-image: url(../assets/Img/sidebar-7.jpg) "></div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">账单</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                       <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-globe"></i>
                                    <b class="caret"></b>
                                    <span class="notification">1</span>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">欢迎来到云端理财</a></li>
                              </ul>
                        </li>           
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                             <a href="logout.aspx">
                                Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row" >
                <div style="margin:0 auto;width:600px;height:300px;border:1px">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">修改支出</h4>
                            </div>
                            <div class="content">
                                <form runat="server">
                                      <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>金额大小</label>
                                                <asp:TextBox ID="box_money" runat="server" CssClass="form-control"></asp:TextBox>    
                                            </div>
                                        </div>
                                         <div class="col-md-3">
                                            <div class="form-group">
                                                <label>种类</label>
                                                <asp:DropDownList ID="List_type" runat="server"  CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>年</label>
                                                <asp:DropDownList ID="List_year" runat="server"  CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>月</label>
                                                  <asp:DropDownList ID="List_month" runat="server"  CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>日</label>
                                                 <asp:DropDownList ID="List_day" runat="server"  CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:button text="确认修改" id="btn_edit"  CssClass="btn btn-info btn-fill pull-right" runat="server" OnClick="btn_edit_Click"></asp:button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                  <ul>
                        <li>
                            <a href="#">
                                About us
                            </a>
                        </li> 
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    Copyright &copy; 2017.YunDuan All rights reserved.
                </p>
            </div>
        </footer>

    </div>
</div>

</body>
 <!--   Core JS Files   -->
    <script src="../assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="../assets/js/bootstrap-checkbox-radio-switch.js"></script>


    <!--  Notifications Plugin    -->
    <script src="../assets/js/bootstrap-notify.js"></script>








</html>



