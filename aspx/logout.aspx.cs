﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class aspx_logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Context.Response.Cookies["login"].Expires = DateTime.Now.AddDays(-1);
        Response.Redirect("../aspx/Login.aspx");
    }
}