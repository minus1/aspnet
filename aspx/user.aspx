﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="user.aspx.cs" Inherits="MasterPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="../assets/css/MasterPage/animate.min.css" rel="stylesheet" />
    <link href="../assets/css/MasterPage/bootstrap.min.css" rel="stylesheet" />
    <link href="../assets/css/MasterPage/light-bootstrap-dashboard.css" rel="stylesheet" />
    <link href="../assets/css/MasterPage/pe-icon-7-stroke.css" rel="stylesheet" />
    <title></title>
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="../assets/Img/sidebar-7.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                云端理财
                </a>
            </div>
            <div class="userimg">
                <div class="profile-main">
                    <div class="profile-pic">
         
                       <ul class="nav">
                           <li id="user_name" class="simple-text">                             
                           你的名字
                           </li>
                           <li>
                               <div style=" margin:10px auto;"><img src="../assets/Img/tim_80x80.png" style=" border-radius:50%"/></div>
                           </li>
                           <li class="simple-text">
                               Welcome
                           </li>
                       </ul>
                  
                    </div>
                </div>
            </div>

            <ul class="nav">
                <li >
                    <a href="statistics.aspx">
                        <i class="pe-7s-graph"></i>
                        <p>统计功能</p>
                    </a>
                </li>
                <li class="active">
                    <a href="#">
                        <i class="pe-7s-user"></i>
                        <p>个人设置</p>
                    </a>
                </li>
                <li>
                    <a href="table.aspx">
                        <i class="pe-7s-note2"></i>
                        <p>账单</p>
                    </a>
                </li>
            </ul>
    	</div>
        <div class="sidebar-background" style="background-image: url(../assets/Img/sidebar-7.jpg) "></div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
               <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" >个人设置</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-dashboard"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-globe"></i>
                                    <b class="caret"></b>
                                    <span class="notification">1</span>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">欢迎来到云端理财</a></li>
                              </ul>
                        </li>
                        <li>
                           <a href="#">
                                <i class="fa fa-search"></i>
                            </a>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                       
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                      Download
                                    <b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">PNG</a></li>
                                <li class="divider"></li>
                                <li><a href="#">DownloadLink</a></li>
                              </ul>
                        </li>
                        <li>
                             <a href="../aspx/logout.aspx">
                                Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                  <div id="user_setting">
                      <input name="user_name" />

                  </div>


                </div>



                
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="#">
                                About us
                            </a>
                        </li> 
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    Copyright &copy; 2017.YunDuan All rights reserved.
                </p>
            </div>
        </footer>

    </div>
</div>




</body>
 <!--   Core JS Files   -->
    <script src="../assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="../assets/js/bootstrap-checkbox-radio-switch.js"></script>


    <!--  Notifications Plugin    -->
    <script src="../assets/js/bootstrap-notify.js"></script>








</html>
