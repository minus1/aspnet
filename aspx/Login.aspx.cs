﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Web.Security;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            /* 第一次进入页面*/
            /*一般读取cookies直接登录页面*/      
        }
        else
        {
            string strCnn = ConfigurationManager.ConnectionStrings["YunDBcstr"].ConnectionString;
            SqlConnection cnn = new SqlConnection(strCnn);
            try
            {
                cnn.Open();
                SqlCommand cmdLogin = new SqlCommand();
                cmdLogin.Connection = cnn;
                ///先找是否存在该用户，若存在则进行验证
                cmdLogin.CommandText = "select count(*) from UserInfo where  Email='" + Request.Form["login_email"]+ "'";
                object count = cmdLogin.ExecuteScalar();
                if((int)count != 0)
                {
                    string password = FormsAuthentication.HashPasswordForStoringInConfigFile(Request.Form["login_password"].Trim(), "md5");
                    cmdLogin.CommandText = "select * from UserInfo where Email='" + Request.Form["login_email"] + "'" + "and UserPw ='" + password + "'";
                    SqlDataReader dr = cmdLogin.ExecuteReader();
                    if(dr.Read())
                    {
                        HttpCookie cookieLogin = new HttpCookie("login");
                        cookieLogin.Value = Request.Form["login_email"];
                        cookieLogin.Expires = System.DateTime.Now.AddDays(7);
                        Response.Cookies.Add(cookieLogin);
                        Response.Redirect("statistics.aspx");
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(ClientScript.GetType(), "login", "<script>  var a = document;b = a.getElementById('login_password'); login.showTip('密码错误',b.nextElementSibling.nextElementSibling);</script>");
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(ClientScript.GetType(), "login", "<script>  var a = document;b = a.getElementById('login_email'); login.showTip('该用户不存在',b.nextElementSibling);</script>");
                }
            }
           catch(Exception ex)
            {
                Response.Write(ex);
            } 
            finally
            {
                if (cnn.State != ConnectionState.Closed)
                {
                    cnn.Close();
                }
            }
            
        }
    }



}