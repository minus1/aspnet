﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SignUp.aspx.cs" Inherits="aspx_SignUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
  
<head runat="server">
  <link rel="stylesheet" href="../assets/css/page/Login.css" type="text/css"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>SignUp</title>
    <script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/login.js"></script>
 
</head>
<body>
    <form id="SignUpForm" runat="server">
    <div class="page-con" style="margin-top:-215px;">
<div><span id="logo-text">云端<label>理财</label></span></div>
<div>

<div style="margin-top:25px;position:relative;">
<input name="signup_email" id="login_email" value="" class="txt" type="text" placeholder="邮箱地址"/>
<div class="error-tip"></div>
</div>
<div class="login-input" style="margin-top:20px;position:relative;">
<input name="signup_pw" id="login_password" class="txt" type="password" placeholder="密码至少6位"/>
<div class="error-tip"></div>
</div>
<div class="login-input" style="margin-top:20px;position:relative;">
<input name="signup_fullname" id="login_fullname" class="txt" type="text" value="" placeholder="请输入不超过15位字符的昵称"/>
<div class="error-tip"></div>
</div>
<div style="margin-top:20px;">
<span id="signin_btn" runat="server" onclick="login.signup()" class="button">立即注册</span>
</div>

</div>
<div class="signup"><a href="Login.aspx">登录</a></div>
<div style="font-size:12px;margin-top:16px;"><span>注册表示您已阅读和同意 <a href="../docs/ser_agreement.html">服务协议</a></span></div>
</div>
    </form>
  
</body>
</html>
