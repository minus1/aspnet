﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="failedOp.aspx.cs" Inherits="aspx_failedOp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>操作失败</title>
    <script type="text/javascript">
        var i = 5;
        window.onload=function page_cg()
        {           
            document.getElementById("time").innerText = i;
            i--;
            if(i<=0)
            {
                location.replace("table.aspx");
            }
            setTimeout(page_cg,1000);
        }
    </script>
    <style type="text/css">
       #AutoJumpPage{
           margin:0 auto;
           width:400px;
           padding:300px;
           height:300px;
           border:1px
       }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="AutoJumpPage">
            操作发生了错误，本页将在<span id="time" style="color: #FF0000"></span>秒后自动跳转至<a href="table.aspx">账单页面</a>
            <br /><br />
            如需立即跳转，请直接点击 <a href="table.aspx" style="color: #20bec8">立即跳转>></a>
        </div>
    </form>
</body>
</html>
