﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class aspx_ModifyData : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["login"] == null)
        {
            Response.Redirect("../aspx/Login.aspx");
        }
        try
        {
            string email = Request.Cookies["login"].Value;
            int id = int.Parse(Request.QueryString["id"]);
            DateTime now = DateTime.Now;
            LinkedList<int> list_allyear = new LinkedList<int>();
            for (int i = 2016; i <= now.Year; i++)
            {
                list_allyear.AddLast(i);
            }
            LinkedList<int> list_allmonth = new LinkedList<int>();
            for (int i = 1; i <= 12; i++)
            {
                list_allmonth.AddLast(i);
            }
            string selectall = "select * from IncomeType";
            SqlConnection str = new SqlConnection(SqlHelper.CONN_STRING);
            SqlDataAdapter da = new SqlDataAdapter(selectall, str);
            DataSet temp = new DataSet();
            da.Fill(temp);
            if (str.State == ConnectionState.Open)
            {
                str.Close();
            }

            if (!IsPostBack)
            {//绑定月份和年份的范围
                List_year.DataSource = list_allyear;
                List_year.DataBind();
                List_month.DataSource = list_allmonth;
                List_month.DataBind();
                //初始化日期
                List_type.SelectedIndex = 0;
                List_year.SelectedValue = now.Year.ToString();
                List_month.SelectedValue = now.Month.ToString();
                BindDay(now.Year, now.Month);
                List_day.SelectedValue = now.Day.ToString();
                List_type.DataSource = temp;
                List_type.DataTextField = "typename";
                List_type.DataValueField = "Id";
                List_type.DataBind();
                List_type.Items.Insert(0, new ListItem("---选择类型---", "0"));
            }
        }
        catch(Exception er)
        {
            Response.Redirect("failedOp.aspx");
        }
    }

    protected void btn_edit_Click(object sender, EventArgs e)
    {
        int t;
        if (!int.TryParse(box_money.Text, out t))
        {
            box_money.Text = "请输入无符号整数";
            box_money.Focus();
            return;
        }
        else if (int.Parse(box_money.Text) < 0)
        {
            box_money.Text = "请输入无符号整数";
            box_money.Focus();
            return;
        }
        if (List_type.SelectedValue == "0")
        {
            box_money.Text = "请选择类型";
            box_money.Focus();
            return;
        }
        string email = Request.Cookies["login"].Value;
        int id = int.Parse(Request.QueryString["id"]);
        DateTime date = new DateTime(int.Parse(List_year.SelectedValue), int.Parse(List_month.SelectedValue), int.Parse(List_day.SelectedValue));
        /* 提交信息*/
        string strCnn = SqlHelper.CONN_STRING;
        SqlConnection cnn = new SqlConnection(strCnn);
        try
        {
            cnn.Open();
            SqlCommand cmdRegister = new SqlCommand();
            cmdRegister.Connection = cnn;
            ///先找是否存在该用户，若存在则进行添加，否则提示错误
            cmdRegister.CommandText = "select count(*) from UserInfo where Email='" + email + "'";
            ///ExecuteScalar方法执行单值查询
            object count = cmdRegister.ExecuteScalar();
            if ((int)count == 0)
            {///非法进入
                Context.Response.Cookies["login"].Expires = DateTime.Now.AddDays(-1);
                Response.Redirect("../aspx/Login.aspx");
            }
            else
            {///利用SqlHelper调用存储过程进行数据插入
                SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "D_EditRecord",
                    new SqlParameter("@email", email),
                    new SqlParameter("@money", int.Parse(box_money.Text.Trim())),
                    new SqlParameter("@type", List_type.SelectedItem.Text.Trim()),
                    new SqlParameter("@date", date.ToShortDateString()),
                    new SqlParameter("@number",id),
                    new SqlParameter("@kind",1)
                    );

            }
        }
        catch (Exception ex)
        {
            Response.Redirect("failedOp.aspx");
        }
        //Response.Write("connection sucecced!");
        finally
        {
            if (cnn.State != ConnectionState.Closed)
            {
                cnn.Close();

            }
        }

        Response.Redirect("table.aspx");
    }


    private void BindDay(int year, int month)
    {
        int i = 0;
        bool flag = false;
        if ((year % 4 == 0) && (year % 100 != 0) || year % 400 == 0)
        {//为闰年
            flag = true;
        }
        else
        {
            flag = false;
        }
        LinkedList<int> list_allday = new LinkedList<int>();
        switch (month)
        {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                for (i = 1; i <= 31; i++)
                    list_allday.AddLast(i);
                break;
            case 2:
                if (!flag)
                {
                    for (i = 1; i <= 29; i++)
                        list_allday.AddLast(i);
                }
                else
                {
                    for (i = 1; i <= 28; i++)
                        list_allday.AddLast(i);
                }
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                for (i = 1; i <= 30; i++)
                    list_allday.AddLast(i);
                break;

        }
        List_day.DataSource = list_allday;
        List_day.DataBind();

    }

    protected void YearChanged_List(object sender, EventArgs e)
    {
        int year = int.Parse(List_year.SelectedValue);
        int month = int.Parse(List_month.SelectedValue);
        BindDay(year, month);
    }

    protected void MonthChanged_List(object sender, EventArgs e)
    {
        int year = int.Parse(List_year.SelectedValue);
        int month = int.Parse(List_month.SelectedValue);
        BindDay(year, month);
        List_day.SelectedValue = "1";
    }

}