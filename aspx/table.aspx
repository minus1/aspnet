﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="table.aspx.cs" Inherits="MasterPage" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
 <meta charset="utf-8" />
<link rel="icon" type="image/png" href="assets/Img/favicon.ico"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="../assets/css/MasterPage/animate.min.css" rel="stylesheet" />
    <link href="../assets/css/MasterPage/bootstrap.min.css" rel="stylesheet" />
    <link href="../assets/css/MasterPage/light-bootstrap-dashboard.css" rel="stylesheet" />
    <link href="../assets/css/MasterPage/pe-icon-7-stroke.css" rel="stylesheet" />
    <title>账单</title>
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="../assets/Img/sidebar-7.jpg">
    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                云端理财
                </a>
            </div>
            <div class="userimg">
                <div class="profile-main">
                    <div class="profile-pic">
         
                       <ul class="nav">
                           <li id="user_name" class="simple-text">                             
                           你的名字
                           </li>
                           <li>
                               <div style=" margin:10px auto;"><img src="../assets/Img/tim_80x80.png" style=" border-radius:50%"/></div>
                           </li>
                           <li class="simple-text">
                               Welcome
                           </li>
                       </ul>
                  
                    </div>
                </div>
            </div>

            <ul class="nav">
                <li>
                    <a href="statistics.aspx">
                        <i class="pe-7s-graph"></i>
                        <p>统计功能</p>
                    </a>
                </li>
                <li>
                    <a href="user.aspx">
                        <i class="pe-7s-user"></i>
                        <p>个人设置</p>
                    </a>
                </li>
                <li class="active">
                    <a href="#">
                        <i class="pe-7s-note2"></i>
                        <p>账单</p>
                    </a>
                </li>
            </ul>
    	</div>
        <div class="sidebar-background" style="background-image: url(../assets/Img/sidebar-7.jpg) "></div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" >账单</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                       <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-globe"></i>
                                    <b class="caret"></b>
                                    <span class="notification">1</span>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">欢迎来到云端理财</a></li>
                              </ul>
                        </li>           
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                             <a href="logout.aspx">
                                Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row" >
                    <form runat="server">
                  <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">收入表</h4>
                                <p class="category">显示所有收入信息</p>
                                        <a href="AddIncomeData.aspx">添加记录</a>
                            </div>
                            <div class="content table-responsive table-full-width">

                        <asp:Repeater ID="IncomeTable_Repeater" runat="server">
                            <HeaderTemplate>
                             <table class="table table-hover table-striped">
                                      <thead> <tr><th>序号</th>
                                          <th>收入类型</th>
                                        <th>金额大小</th>
                                    	<th>日期</th>
                                    	<th>操作</th>
                                    </tr></thead></HeaderTemplate>
                                <ItemTemplate>
                                        <tr>
                                            <td><%#IncomePagerShow.PageSize * IncomePagerShow.CurrentPageIndex + Container.ItemIndex -IncomePagerShow.PageSize+1%>  </td>
                                        	 <td><%#DataBinder.Eval(Container.DataItem,"type")%></td>
                                                <td>+<%#DataBinder.Eval(Container.DataItem,"IncomeMoney")%></td>
                                                <td><%#DataBinder.Eval(Container.DataItem,"InDate","{0:d}")%></td>           
                                                <td>
                                                 <a href="EidtIncome.aspx?id=<%#DataBinder.Eval(Container.DataItem,"Id")%>">修改</a>
                                                 <a class="D_delete" href="DelData.aspx?id=<%#DataBinder.Eval(Container.DataItem,"Id")%>&kind=1">删除</a>
                                                </td>
                                        </tr>
                                </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>       
                        </asp:Repeater>                               
                                 <div class="footer">
                                    <webdiyer:AspNetPager ID="IncomePagerShow" runat="server" AlwaysShow="true"                
                                   CurrentPageButtonClass="cpb"
                                         Urlpaging="true"
                                        CustomInfoTextAlign="Right"
                                        FirstPageText="首页"
                                        pagesize="6"
                                        Font-Size="12px"
                                        ForeColor="Black"
                                        LastPageText="尾页" NextPageText="下一页"
                                        NumericButtonCount="100"
                                        OnPageChanging="IncomePagerShow_PageChanged"
                                        PrevPageText="上一页"
                                        ReverseUrlPageIndex="True"
                                        ShowCustomInfoSection="Left"
                                        ShowFirstLast="true"
                                        ShowPageIndexBox="Never"
                                        LayoutType="Table"
                                        Width="100%" 
                                        OnPageChanged="IncomePagerShow_PageChanged">
                                    </webdiyer:AspNetPager>
                                 </div>
                            </div>
                        </div>
                    </div>
                  <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">支出表</h4>
                                <p class="category">显示所有支出信息</p>
                                 <a href="AddExpendData.aspx">添加记录</a>
                            </div>
                            <div class="content table-responsive table-full-width">
                              <asp:Repeater ID="ExpendTable_Repeater" runat="server">
                            <HeaderTemplate>
                             <table class="table table-hover table-striped">
                                    <thead>
                                       <tr><th>序号</th>
                                           <th>支出类型</th>
                                        <th>金额大小</th>
                                    	<th>日期</th>
                                    	<th>操作</th>
                                    </tr></thead></HeaderTemplate>           
                                          <ItemTemplate>
                                        <tr>
                                            <td><%#IncomePagerShow.PageSize * IncomePagerShow.CurrentPageIndex + Container.ItemIndex -IncomePagerShow.PageSize+1%>  </td>
                                        	    <td><%#DataBinder.Eval(Container.DataItem,"type")%></td>
                                                <td>-<%#DataBinder.Eval(Container.DataItem,"ExpendMoney")%></td>
                                                <td><%#DataBinder.Eval(Container.DataItem,"OutDate","{0:d}")%></td>           
                                                <td>
                                                <a href="EditExpend.aspx?id=<%#DataBinder.Eval(Container.DataItem,"Id")%>">修改</a>
                                                 <a class="D_delete" href="DelData.aspx?id=<%#DataBinder.Eval(Container.DataItem,"Id")%>&kind=2">删除</a>
                                                </td>
                                        </tr>
                                          </ItemTemplate>                       
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>    
                            
                        </asp:Repeater>                               
                                 <div class="footer">
                                    <webdiyer:AspNetPager ID="ExpendPagerShow" runat="server" AlwaysShow="true"                
                                        CurrentPageButtonClass="cpb"
                                         Urlpaging="true"
                                        CustomInfoTextAlign="Right"
                                        FirstPageText="首页"
                                        Font-Size="12px"
                                        ForeColor="Black"
                                        LastPageText="尾页" NextPageText="下一页"
                                        NumericButtonCount="100"
                                        OnPageChanging="ExpendPagerShow_PageChanged"
                                        PrevPageText="上一页"
                                        ReverseUrlPageIndex="True"
                                        ShowCustomInfoSection="Left"
                                        ShowFirstLast="true"
                                        ShowPageIndexBox="Never"
                                        LayoutType="Table"
                                        OnPageChanged="ExpendPagerShow_PageChanged"
                                        PageSize="6"
                                        Width="100%">
                                    </webdiyer:AspNetPager>
                                 </div>
                            </div>
                        </div>
                    </div>
               </form>
                         </div>    
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                  <ul>
                        <li>
                            <a href="#">
                                About us
                            </a>
                        </li> 
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    Copyright &copy; 2017.YunDuan All rights reserved.
                </p>
            </div>
        </footer>

    </div>
</div>



</body>
 <!--   Core JS Files   -->
    <script src="../assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $(".D_delete").click(function () {
                return confirm("确定删除吗？");
            });
        });
    </script>
	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="../assets/js/bootstrap-checkbox-radio-switch.js"></script>


    <!--  Notifications Plugin    -->
    <script src="../assets/js/bootstrap-notify.js"></script>








</html>
