﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

public partial class MasterPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["login"] == null)
        {
            Response.Redirect("../aspx/Login.aspx");
        }
        if (!IsPostBack)
        {
            int Income_totalOrders = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "P_GetIncomeTotal",
              new SqlParameter("@email", Request.Cookies["login"].Value.Trim()));
            IncomePagerShow.RecordCount = Income_totalOrders;
            int Expend_totalOrders = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "P_GetExpendTotal",
                 new SqlParameter("@email", Request.Cookies["login"].Value.Trim()));
            ExpendPagerShow.RecordCount = Expend_totalOrders;
             //Response.Write(ExpendPagerShow.StartRecordIndex);
        }
    }

    void IncomebindData()
    {

        IncomeTable_Repeater.DataSource = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "P_GetIncomeOrderNumber",
             new SqlParameter("@startRecordIndex", IncomePagerShow.PageSize*(IncomePagerShow.CurrentPageIndex-1)+1),
             new SqlParameter("@endRecordIndex", IncomePagerShow.PageSize*IncomePagerShow.CurrentPageIndex),
            new SqlParameter("@email", Request.Cookies["login"].Value.Trim())
            );
        IncomeTable_Repeater.DataBind();

    }

    void ExpendbindData()
    {

       ExpendTable_Repeater.DataSource = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "P_GetExpendOrderNumber",
           new SqlParameter("@startRecordIndex", ExpendPagerShow.PageSize * (ExpendPagerShow.CurrentPageIndex - 1) + 1),
           new SqlParameter("@endRecordIndex", ExpendPagerShow.PageSize * ExpendPagerShow.CurrentPageIndex),
          new SqlParameter("@email", Request.Cookies["login"].Value.Trim())
          );
        ExpendTable_Repeater.DataBind();
    }

    protected void IncomePagerShow_PageChanged(object src, EventArgs e)
    {
        IncomebindData();
    }

    protected void ExpendPagerShow_PageChanged(object src, EventArgs e)
    {
        ExpendbindData();
    }


}