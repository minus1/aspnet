﻿<%@ Page EnableViewState="true" Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html>
<link href="../assets/css/page/Login.css" rel="stylesheet" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Welcome</title>
       <style type="text/css">
   

        .bg {
            background: url('../assets/Img/sidebar-4.jpg');
            height: 600px;
            text-align: center;
            line-height: 600px;
        }

        .bg-blur {
            float: left;
            width: 100%;
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            -webkit-filter: blur(15px);
            -moz-filter: blur(15px);
           
            -ms-filter: blur(15px);
            filter: blur(15px);
        }    
    </style>
    <script src="../assets/js/login.js"></script>

</head>
<body>
 
      <div class="bg bg-blur "></div>
           <div class="page-con content-front"> 
<div ><span id="logo-text" style="color:#ffffff">云端<label>理财</label></span></div>
<div>
   <form id="LoginForm" runat="server" method="post">
<div style="margin-top:25px;position:relative;">
<input name="login_email" id="login_email" value="" class="txt" type="text" placeholder="账户/邮箱地址"/>
<div class="error-tip"></div>
</div>
<div class="login-input" style="margin-top:20px;position:relative;">
<input name="login_password" id="login_password" class="txt" type="password" placeholder="密码"/>
<a href="SignUp.aspx ">忘记密码</a>
<div class="error-tip"></div>
</div>
<div style="margin-top:20px;"  >
<span id="signin_btn"  onclick="login.login()" class="button" >立即登录</span>
</div>
    </form>
</div>
<div class="signup"><a href="SignUp.aspx" style="color:white">注册</a></div>

</div>

</body>
</html>
