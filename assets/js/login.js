﻿var login = {
   showTip: function (a, b) {
        b.innerHTML = a,
        setTimeout(function () {
            b.innerHTML = ""
        },
        3e3)
    },
    isLogining: !1,
    login: function () {
        if (!login.isLogining) {
            var a = document,
            b = a.getElementById("login_email"),
            c = a.getElementById("login_password");
            if ("" == b.value.trim()) return b.focus(),
            void login.showTip("邮箱不能为空", b.nextElementSibling);
            if (!/^([\w]+)(.[\w]+)*@([\w-]+\.){1,5}([A-Za-z]){2,4}$/.test(b.value.trim())) return b.focus(),
            void login.showTip("邮箱格式不正确", b.nextElementSibling);
            if ("" == c.value.trim()) return c.focus(),
            void login.showTip("密码不能为空", c.parentNode.querySelector(".error-tip"));
            b.nextElementSibling.innerHTML = "",
            c.parentNode.querySelector(".error-tip").innerHTML = "";
            login.isLogining = !0,
            a.getElementById("login_email").value = b.value.trim(),
            a.getElementById("login_password").value = c.value.trim(),
            document.getElementById("LoginForm").submit()
        }
    },
    signup: function () {
        if (!login.isLogining) {
            var a = document,
            b = a.getElementById("login_email"),
            c = a.getElementById("login_password"),
            d = a.getElementById("login_fullname");
            if ("" == b.value.trim()) return b.focus(),
            void login.showTip("邮箱不能为空", b.nextElementSibling);
            if (!/^([\w]+)(.[\w]+)*@([\w-]+\.){1,5}([A-Za-z]){2,4}$/.test(b.value.trim())) return b.focus(),
            void login.showTip("邮箱格式不正确", b.nextElementSibling);
            if ($.inArray($.trim(b.value), login.tempmails) >= 0) return b.focus(),
            void login.showTip("此类型邮箱暂不支持", b.nextElementSibling);
            if ("" == c.value.trim()) return c.focus(),
            void login.showTip("密码不能为空", c.nextElementSibling);
            if ("" == d.value.trim()) return d.focus(),
            void login.showTip("昵称不能为空", d.nextElementSibling);
            if (d.value.length > 15) return d.focus(),
            void login.showTip("昵称最大长度为15位", d.nextElementSibling);
            a.getElementById("login_email").value = b.value.trim(),
            a.getElementById("login_password").value = c.value.trim(),
            a.getElementById("login_fullname").value = d.value.trim(),
            b.nextElementSibling.innerHTML = "",
            c.nextElementSibling.innerHTML = "",
            d.nextElementSibling.innerHTML = "";
            login.isLogining = !0,
       
            document.getElementById("SignUpForm").submit()
        }
    },
    bind: function () {
        var a = $("#login_email"),
        b = $("#login_password"),
        c = "^.{6,24}$",
        d = document.getElementById("login_email"),
        e = document.getElementById("login_password");
        if ("" == $.trim(a.val()) || 0 == $.trim(a.val()).isEmail() || "" == $.trim(b.val())) {
            if ("" == $.trim(a.val()) || 0 == $.trim(a.val()).isEmail()) {
                a.focus();
                var d = document.getElementById("login_email");
                return login.showTip("邮箱格式不正确", d.nextElementSibling),
                !1
            }
            if ("" == $.trim(b.val())) return b.focus(),
            login.showTip("请输入密码", e.nextElementSibling),
            !1
        } else {
            if ("" == $.trim(a.val()) || !$.trim(a.val()).isEmail() || "" == $.trim(b.val())) return a.focus(),
            login.showTip("邮箱格式不正确", d.nextElementSibling),
            !1;
            if (!$.trim(b.val()).match(c)) return b.focus(),
            login.showTip("密码格式错误", e.nextElementSibling),
            !1;
            a.val($.trim(a.val())),
            b.val($.trim(b.val())),
            $("#signin_bind_form").submit()
        }
    },
    tempmails: ["mvrht.com", "34nm.com", "dingbone.com", "fudgerub.com", "lookugly.com", "smellfear.com", "tempinbox.com", "yopmail.com", "yopmail.fr", "yopmail.net", "cool.fr.nf", "jetable.fr.nf", "nospam.ze.tc", "nomail.xl.cx", "mega.zik.dj", "speed.1s.fr", "courriel.fr.nf", "moncourrier.fr.nf", "monemail.fr.nf", "monmail.fr.nf", "chacuo.net", "027168.com", "www.bccto.me", "mail.bccto.me", "sharklasers.com", "guerrillamail.info", "grr.la", "guerrillamail.biz", "guerrillamail.com", "guerrillamail.de", "guerrillamail.net", "guerrillamail.org", "guerrillamailblock.com", "pokemail.net", "spam4.me", "nowmymail.com", "mailcatch.com", "incognitomail.org", "cd.mintemail.com", "spamgourmet.com", "e4ward.com", "mailinator.com", "spamfree24.org", "mt2015.com", "mailnesia.com", "tempemail.net", "maildrop.cc"]
};
